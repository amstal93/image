FROM BASE_IMAGE

RUN curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain RUST_TOOLCHAIN

# .cargo/bin in PATH is needed for running cargo, rustup etc.
ENV PATH=/root/.cargo/bin:$PATH

# A tool for visualizing Rust build dependencies. Handy for debugging the build inside the container.
RUN cargo install cargo-tree

# Installing the musl target!
RUN rustup target add x86_64-unknown-linux-musl

# SETTING THE ENV VARS FOR MUSL BUILDS - WHY THESE?
# For pkg-config (used by many libraries, including pq-sys, but only in the case its pkg_config feature is enabled):
#   PKG_CONFIG_ALLOW_CROSS must be set for cross-complation to be allowed
#   PKG_CONFIG_ALL_STATIC must be set for static linking
#   TARGET, HOST must be set for cross-compilation to work
#   PKG_CONFIG_PATH_x86_64_unknown_linux_musl and PKG_CONFIG_PATH_x86_64_unknown_linux_gnu must be set for cross-compilation to work
# For pq-sys (pq-sys uses pg_config by default)
#   PATH /musl/bin is good to set in case some libraries run external config tools in their build scripts
#   PQ_LIB_STATIC_X86_64_UNKNOWN_LINUX_MUSL to enable static linking
#   PG_CONFIG_X86_64_UNKNOWN_LINUX_MUSL to enable finding pg_config for the musl library
# For OpenSSL:
#   OPENSSL_STATIC to enable static linking
#   OPENSSL_DIR because OpenSSL's idiosyncratic build system
#   SSL_CERT_FILE, SSL_CERT_DIR for finding certificates
# For Libz:
#   LIBZ_SYS_STATIC to enable static linking

ENV PATH=$MUSL_PREFIX/bin:$PATH \
    TARGET=x86_64_unknown_linux_musl \
    HOST=x86_64_unknown_linux_gnu \
    PKG_CONFIG_ALLOW_CROSS=1 \
    PKG_CONFIG_ALL_STATIC=true \
    PKG_CONFIG_PATH_x86_64_unknown_linux_musl=$MUSL_PREFIX/lib/pkgconfig \
    PKG_CONFIG_PATH_x86_64_unknown_linux_gnu=/usr/lib/x86_64-linux-gnu/pkgconfig \
    PQ_LIB_STATIC_X86_64_UNKNOWN_LINUX_MUSL=true \
    PG_CONFIG_X86_64_UNKNOWN_LINUX_MUSL=/musl/bin/pg_config \ 
    OPENSSL_STATIC=true \
    OPENSSL_DIR=$MUSL_PREFIX \
    SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt \
    SSL_CERT_DIR=/etc/ssl/certs \
    LIBZ_SYS_STATIC=1
    
WORKDIR /workdir
