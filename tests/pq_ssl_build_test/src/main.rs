extern crate native_tls;
extern crate pq_sys;

pub fn main() {
    let _connector = native_tls::TlsConnector::new().unwrap();
    let _result = unsafe { pq_sys::PQconnectStart(&*b"test" as *const u8 as *const i8)};
}
